// #include <beast/core/multi_buffer.hpp>
// #include <beast/core/buffers_adapter.hpp>
#include <beast/core.hpp>
#include <beast/http.hpp>
#include <boost/asio.hpp>

int main() {
    boost::asio::io_service service;
    boost::asio::ip::tcp::socket stream{service};
    beast::http::parser<false, beast::http::dynamic_body> parser;

    using cache_type = boost::asio::streambuf;

    cache_type streambuf;
    beast::buffers_adapter<cache_type::mutable_buffers_type> cache(
        streambuf.prepare(512));
    // works
    beast::http::read(stream, cache, parser);

    // does not work
    beast::http::read(stream,
                      beast::buffers_adapter<cache_type::mutable_buffers_type>(
                          streambuf.prepare(512)),
                      parser);

    return 0;
}


